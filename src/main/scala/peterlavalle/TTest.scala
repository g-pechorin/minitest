package peterlavalle

import org.scalatest.funsuite.AnyFunSuite

import java.io.{File, FileWriter, Writer}
import java.util.Random
import scala.io.{BufferedSource, Source}

trait TTest extends AnyFunSuite with TemplateResource {

	private lazy val startup: Long = System.currentTimeMillis()
	private lazy val target: File = new File("target").getAbsoluteFile

	def benchmark[O](name: String)(action: => O): O = {
		val start: Double = age()
		println(s"[$name] ; starting ...")
		val out = action
		val total: Double = age() - start
		println(s"[$name] ; ... done ($total)")
		out
	}

	protected def age(): Double = (System.currentTimeMillis() - startup) * 0.001

	def assertReSourceEqual(expected: String)(actual: Seq[String]): Unit = {
		assertSourceEqual(resourceStream(expected), actual)
	}

	def assertSourceEqual(expected: Seq[String], actual: Seq[String]): Unit =
		assertSourceEqual(
			expected.foldLeft("")((_: String) + (_: String) + "\n"),
			actual.foldLeft("")((_: String) + (_: String) + "\n"),
		)

	implicit class extendRandom(random: Random) {
		def seed(i: Long) = new Random(random.nextLong() ^ i)

		def nextByte() = {
			val data = Array.ofDim[Byte](1)
			random.nextBytes(data)
			data.head
		}
	}

	def assertSourceEqual(expected: String, actual: String): Unit = {
		def normalise: String => String =
			normalisers.reverse.foldRight(_: String)(_ apply _)

		lazy val traceCode: String =
			getClass.getName + Math.abs(
				Thread.currentThread()
					.getStackTrace
					.toList
					.foldLeft("")((_: String) + (_: StackTraceElement))
					.hashCode
			).toString


		val ev: String = normalise(expected)
		val av: String = normalise(actual)

		val ef: File =
			target / (traceCode + ".expected") ioWriteLines expected

		val af: File =
			target / (traceCode + ".actual") ioWriteLines actual

		if (ev != av) {
			System.err.println(
				"kdiff3" +
					" " +
					ef.AbsolutePath +
					" " +
					af.AbsolutePath
			)
			assert(av == ev)
		}
	}

	def normalisers =
		List(
			(_: String).replaceAll("([\t \r]*\n)+", "\n"),
			(_: String).trim
		)

	def resourceStream(name: String): Stream[String] = {
		bind(name) {
			name =>
				fail(s"there should be no templates, but, asked for $name")
		}
	}

	def randomTest(name: String)(f: Random => Unit): Unit = test(name)(seeded(f))

	def seeded(
							test: Random => Unit,

							/**
							 * i'll run at least this many tests in total
							 */
							count: Int = env("seeded.count", 38).toInt,
						): Unit = {

		val file =
			("target" / "seeded" / Thread.currentThread()
				.getStackTrace
				.tail
				.foldLeft("")((_: String) + (_: StackTraceElement)).md5)
				.EnsureParent


		// always start with old ones
		val seen: Stream[Long] =
			if (!file.exists())
				Stream()
			else
				Source.fromFile(file).using(
					(_: BufferedSource)
						.mkString
						.split("[\r \t]*\n")
						.toStream
						.map((_: String).takeWhile('#' != (_: Char)).trim)
						.filter((_: String).nonEmpty)
						.map(java.lang.Long.parseLong(_: String, 16))
				)

		// here's the "full" suite
		val full: Stream[Long] =
			seen ++ Stream.continually(new Random().nextLong()).take(Math.max(0, count + seen.size))

		// test them all
		full.foreach {
			seed: Long =>
				try {
					test(new Random(seed))
				} catch {
					case e: Throwable =>
						val hash: String = java.lang.Long.toString(seed, 16)
						// move the seed to the top of the file
						(hash #:: seen.filterNot((_: Long) == seed).map(java.lang.Long.toString(_: Long, 16)))
							.foldLeft(new FileWriter(file): Writer)(
								(_: Writer) append (_: String) append "\n"
							)
							.close()

						// re-throw, but, copy the details from the real exception
						val runtimeException = new RuntimeException(s"(seed=$hash)" + e.getMessage, e)
						runtimeException.setStackTrace(e.getStackTrace)
						throw runtimeException
				}
		}
	}

	def env(key: String, default: Any): String = {
		val env: String = System.getenv(key)
		System.getProperty(
			key,
			if (null == env)
				default.toString
			else
				env
		)
	}

	def resourceString(name: String): String =
		resourceStream(name).foldLeft("")(_ + _ + "\n")
}
